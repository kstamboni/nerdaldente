from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from .util.jsend import *
from core.models import *
from .forms import *
from django.http import JsonResponse
from django.core.exceptions import PermissionDenied
import hashlib
import json
from decimal import *

# Create your views here.

def validar_usuario(request):
    '''
        Informa se o e-mail informado pelo usuário já está registrado ou não.
    '''
    if request.method == 'POST':
        try:
            username = request.POST['email']
        except KeyError:
            return JsonResponse(JSend.fail({"email": "Campo requerido."}))
        data = {
            'existe': User.objects.filter(username__iexact=username).exists()
        }
        return JsonResponse(JSend.success(data))
    else:
        raise PermissionDenied

def upload_imagens(request, pk):
    '''
        Responsável por receber as imagens de uma receita cadastrada pelo usuário.
    '''
    if request.method == 'POST':
        try:
            receita = Receita.objects.get(id=pk)
        except Receita.DoesNotExist:
            raise PermissionDenied

        if(request.user.id != receita.autor.dj_usuario.id):
            raise PermissionDenied

        form = ImagemForm(request.POST, request.FILES)
        if form.is_valid():
            imagem = form.save(commit=False)
            imagem.imagem_alt = "Imagem ilustrativa da receita."
            imagem.receita = receita
            imagem.save()
            return JsonResponse(JSend.success({'ok': True,
                                               'mensagem': '"' + request.FILES['file'].name + '" enviado com sucesso.'}))
        else:
            return JsonResponse(JSend.fail({'ok': False,
                                            'mensagem': '"' + request.FILES['file'].name + '" não é um arquivo de imagem válido.'}))
    else:
        raise PermissionDenied

def cadastrar_multiplos(request):
    '''
        Responsável pelo cadastro de campos com múltiplos dados (categorias e ingredientes).
    '''
    if request.method == 'POST':
        try:
            id_receita = request.POST['id']
        except KeyError:
            return JsonResponse(JSend.fail({"id": "Campo requerido."}))

        try:
            receita = Receita.objects.get(id=id_receita)
        except Receita.DoesNotExist:
            raise PermissionDenied

        categorias = request.POST.getlist('categorias[]')
        ingredientes = request.POST.getlist('ingredientes')

        if (len(categorias) == 0) or (len(ingredientes) == 0):
            return JsonResponse(JSend.fail({"erro": "Listas vazias de categorias ou ingredientes."}))

        ingredientes = json.loads(ingredientes[0])

        for categoria in categorias:
            try:
                cat = Categoria.objects.get(nome_categoria=categoria)
            except Categoria.DoesNotExist:
                raise PermissionDenied

            rec_cat = ReceitaCategoria(receita=receita, categoria=cat)
            rec_cat.save()

        for ingrediente in ingredientes:
            nome = ingrediente["nome_ingrediente"].lower()
            try:
                ing = Ingrediente.objects.get(nome_ingrediente=nome)
            except Ingrediente.DoesNotExist:
                ing = Ingrediente(nome_ingrediente=nome)
                ing.save()

            try:
                unidade = Unidade.objects.get(nome_unidade=ingrediente["unidade"])
            except Unidade.DoesNotExist:
                raise PermissionDenied

            # Para preservar a semântica, algumas unidades só admitem o valor 1.
            if (ingrediente["unidade"] == "pitada") or (ingrediente["unidade"] == "pouco"):
                quantidade = float(1)
            else:
                quantidade = float(ingrediente["quantidade"])

            rec_ing = ReceitaIngrediente(receita=receita, ingrediente=ing, quantidade=quantidade, unidade=unidade)
            rec_ing.save()

        return JsonResponse(JSend.success({'ok': True}))
    else:
        raise PermissionDenied

def cadastrar_receita(request):
    '''
        Cadastra os demais dados de uma nova receita.
    '''
    if request.method == 'POST':
        try:
            id_receita = request.POST['id']
        except KeyError:
            return JsonResponse(JSend.fail({"id": "Campo requerido."}))
        try:
            nome_receita = request.POST['nome_receita']
        except KeyError:
            return JsonResponse(JSend.fail({"nome_receita": "Campo requerido."}))
        try:
            descricao = request.POST['descricao']
        except KeyError:
            return JsonResponse(JSend.fail({"descricao": "Campo requerido."}))
        try:
            instrucoes = request.POST['instrucoes']
        except KeyError:
            return JsonResponse(JSend.fail({"instrucoes": "Campo requerido."}))
        try:
            tempo = request.POST['tempo']
        except KeyError:
            return JsonResponse(JSend.fail({"tempo": "Campo requerido."}))
        try:
            porcoes = request.POST['porcoes']
        except KeyError:
            return JsonResponse(JSend.fail({"porcoes": "Campo requerido."}))
        try:
            nutricional = request.POST['nutricional']
        except KeyError:
            return JsonResponse(JSend.fail({"nutricional": "Campo requerido."}))
        try:
            metodo = request.POST['metodo']
        except KeyError:
            return JsonResponse(JSend.fail({"metodo": "Campo requerido."}))
        try:
            receita = Receita.objects.get(id=id_receita)
        except Receita.DoesNotExist:
            raise PermissionDenied

        try:
            metodo = MetodoCozimento.objects.get(nome_metodo=metodo)
        except MetodoCozimento.DoesNotExist:
            raise PermissionDenied

        try:
            imagem = Imagem.objects.filter(receita=receita)
        except Imagem.DoesNotExist:
            raise PermissionDenied

        receita.nome_receita = nome_receita
        receita.descricao = descricao
        receita.tempo_preparo = tempo
        receita.instrucoes_preparo = instrucoes
        receita.porcoes = porcoes
        receita.valor_nutricional = nutricional
        receita.metodo_cozimento = metodo
        receita.imagem_padrao = imagem[0]
        receita.save()

        return JsonResponse(JSend.success({'ok': True}))
    else:
        raise PermissionDenied

def cadastrar_usuario(request):
    '''
        Responsável por receber os dados de um novo usuário.
    '''
    if request.method == 'POST':
        try:
            nome = request.POST['nome']
        except KeyError:
            return JsonResponse(JSend.fail({"nome": "Campo requerido."}))
        try:
            dt_nascimento = request.POST['dt_nascimento']
        except KeyError:
            return JsonResponse(JSend.fail({"dt_nascimento": "Campo requerido."}))
        try:
            cidade = request.POST['cidade']
        except KeyError:
            return JsonResponse(JSend.fail({"cidade": "Campo requerido."}))
        try:
            estado = request.POST['estado']
        except KeyError:
            return JsonResponse(JSend.fail({"estado": "Campo requerido."}))
        try:
            telefone = request.POST['telefone']
        except KeyError:
            return JsonResponse(JSend.fail({"telefone": "Campo requerido."}))
        try:
            email = request.POST['email']
        except KeyError:
            return JsonResponse(JSend.fail({"email": "Campo requerido."}))
        try:
            senha = request.POST['senha']
        except KeyError:
            return JsonResponse(JSend.fail({"senha": "Campo requerido."}))

        nome_s = nome.split()
        last_name = nome_s[len(nome_s)-1][0] + '.'

        dj_user = User(username=email, first_name=nome_s[0].title(), last_name=last_name, password=make_password(senha))
        dj_user.save()
        usuario = Usuario(dj_usuario=dj_user, nome_completo=nome, data_nascimento=dt_nascimento, cidade=cidade, estado=estado, telefone=telefone)
        usuario.save()

        return JsonResponse(JSend.success({'ok': True}))
    else:
        raise PermissionDenied
