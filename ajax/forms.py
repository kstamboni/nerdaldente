from django import forms
from core.models import Imagem

class ImagemForm(forms.ModelForm):
	'''
		Formulário para upload de múltiplas imagens (uma por uma).
	'''
	class Meta:
		model = Imagem
		fields = ('file', )
