from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^validar-usuario/$', views.validar_usuario, name='validar_usuario'),
    url(r'^cadastrar-usuario/$', views.cadastrar_usuario, name='cadastrar_usuario'),
    url(r'^cadastrar-receita/$', views.cadastrar_receita, name='cadastrar_receita'),
    url(r'^cadastrar-multiplos/$', views.cadastrar_multiplos, name='cadastrar_multiplos'),
    url(r'^upload-imagens/(?P<pk>\d+)$', views.upload_imagens, name='upload_imagens'),
]
