from django.shortcuts import render, redirect
from django.http import Http404, HttpResponseServerError
from .models import *
import random, datetime
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.db.models import Count
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from django.db.models import Count, Avg
from django.utils import timezone

# Create your views here.

def get_receitas(query="", source="index"):
    '''
        Método auxiliar para obtenção de uma lista de receitas em vários contextos da aplicação.
    '''
    if source == "index":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).filter(categorias=query).order_by('-data_publicacao')[:3]
        if len(receitas) < 3:
            return None
        return receitas
    elif source == "cat":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).filter(categorias=query).order_by('-data_publicacao')[:10]
        return receitas
    elif source == "search":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).filter(Q(autor__nome_completo__icontains=query) | Q(nome_receita__icontains=query) | Q(descricao__icontains=query) | Q(instrucoes_preparo__icontains=query) | Q(metodo_cozimento__nome_metodo__icontains=query) | Q(categorias__nome_categoria__icontains=query) | Q(ingredientes__nome_ingrediente__icontains=query)).distinct().order_by('-data_publicacao')[:10]
        return receitas
    elif source == "best_small":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).order_by('-media_notas')[:10]
        return receitas
    elif source == "best_big":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).order_by('-media_notas')[:5]
        return receitas
    elif source == "recent":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).order_by('-data_publicacao')[:5]
        return receitas
    elif source == "suggestions":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).values('nome_receita').distinct().order_by('-media_notas', '-data_publicacao')[:30]
        return receitas
    else:
        return None

def get_usuarios(type):
    '''
        Obter os usuários mais ativos no site para as listas na página inicial.
    '''
    if type == "degustadores":
        usuarios = Comentario.objects.exclude(receita__imagem_padrao__isnull=True).values('autor__first_name', 'autor__last_name').annotate(total=Count('autor')).order_by('-total')[:5]
        return usuarios
    elif type == "chefs":
        usuarios = Receita.objects.exclude(imagem_padrao__isnull=True).values('autor__dj_usuario__first_name', 'autor__dj_usuario__last_name').annotate(total=Count('autor')).order_by('-total')[:5]
        return usuarios
    return None

# Páginas HTML
def index(request):
    '''
        Cria a página inicial com todos os dados necessários.
    '''

    # Exclui as receitas que expiraram e não foram concluídas.
    Receita.objects.exclude(imagem_padrao__isnull=False).filter(data_publicacao__lte=timezone.now() - datetime.timedelta(minutes=1440)).delete()

    categorias = Categoria.objects.all()
    vet = random.sample(range(0, Categoria.objects.count()), 4)
    data = {
        "categoria1": categorias[vet[0]],
        "receitas1": get_receitas(categorias[vet[0]]),
        "categoria2": categorias[vet[1]],
        "receitas2": get_receitas(categorias[vet[1]]),
        "categoria3": categorias[vet[2]],
        "receitas3": get_receitas(categorias[vet[2]]),
        "categoria4": categorias[vet[3]],
        "receitas4": get_receitas(categorias[vet[3]]),
        "sugestao_receitas": get_receitas(source="suggestions"),
        "chefs": get_usuarios("chefs"),
        "degustadores": get_usuarios("degustadores"),
    }
    return render(request, 'index.html', data)

def login_view(request):
    '''
        Cria o formulário de login e recebe as credenciais enviadas.
    '''
    if request.method == 'POST':
        usuario = authenticate(username=request.POST['email'], password=request.POST['senha'])
        if usuario is not None:
            login(request, usuario)
            if request.POST['next']:
                return redirect(request.POST['next'] + '?from=/login/')
            return redirect('/?ln=ok')
        else:
            return redirect('/login/?ln=fail')
    data = {
        "sugestao_receitas": get_receitas(source="suggestions"),
    }
    return render(request, 'login.html', data)

def logout_view(request):
    '''
        Finaliza a sessão.
    '''
    logout(request)
    return redirect('/?lo=ok')

def detalhes_receita(request, pk):
    '''
        Cria a página com todos os detalhes de uma receita, incluindo suas imagens e comentários recebidos.
    '''
    try:
        receita = Receita.objects.exclude(imagem_padrao__isnull=True).get(id=pk)
    except Receita.DoesNotExist:
        raise Http404

    imagens = Imagem.objects.filter(receita=receita)
    comentarios = Comentario.objects.filter(receita=receita)
    data = {
        'id_receita': pk,
        'url': "https://nerdaldente.com.br/receita/" + pk,
        'receita': receita,
        'autor': receita.autor.nome_completo.title(),
        'imagens': imagens,
        'comentarios': comentarios,
        "sugestao_receitas": get_receitas(source="suggestions"),
        }

    if request.method == 'POST':
        try:
            autor = User.objects.get(id=request.user.id)
        except User.DoesNotExist:
            raise PermissionDenied

        try:
            receita = Receita.objects.get(id=pk)
        except Receita.DoesNotExist:
            raise PermissionDenied

        try:
            nota = request.POST['nota']
        except KeyError:
            return JsonResponse(JSend.fail({"nota": "Campo requerido."}))

        try:
            comentario = request.POST['comentario']
        except KeyError:
            return JsonResponse(JSend.fail({"comentario": "Campo requerido."}))

        comentario = Comentario(autor=autor, nota=nota, receita=receita, comentario=comentario)
        comentario.save()

        media_notas = Comentario.objects.filter(receita=receita).exclude(nota=0).aggregate(Avg('nota'))
        qtd_votos = Comentario.objects.filter(receita=receita).exclude(nota=0).aggregate(Count('nota'))

        receita.media_notas = float(media_notas["nota__avg"])
        receita.qtd_votos = qtd_votos["nota__count"]
        receita.save()

    return render(request, 'detalhes.html', data)

def imprimir_receita(request, pk):
    '''
        Cria a página para ser impressa com os detalhes mais relevantes de uma receita.
    '''

    try:
        receita = Receita.objects.exclude(imagem_padrao__isnull=True).get(id=pk)
    except Receita.DoesNotExist:
        raise Http404
    data = {
        'id_receita': pk,
        'url': "https://nerdaldente.com.br/imprimir/" + pk,
        'receita': receita,
        'autor': receita.autor.nome_completo.title(),
        }
    return render(request, 'imprimir.html', data)

def categoria_receita(request, string):
    '''
        Lista as receitas mais recentes de uma categoria.
    '''

    try:
        categoria = Categoria.objects.get(nome_categoria=string.title())
    except Categoria.DoesNotExist:
        raise Http404
    data = {
        'categoria': string.title(),
        'url': "https://nerdaldente.com.br/categoria/" + string,
        'receitas': get_receitas(categoria, "cat"),
        'receitas_aside': get_receitas(source="best_big"),
        "sugestao_receitas": get_receitas(source="suggestions"),
    }
    return render(request, 'listar_categoria.html', data)

def pesquisar(request):
    '''
        Lista as receitas que se adequam à string de pesquisa do usuário.
    '''

    if request.GET.get('q'):
        q = str(request.GET.get('q')).lower()
    else:
        return render(request, 'teste.html')
    data = {
        'q': q,
        'url': "https://nerdaldente.com.br/pesquisar/" + q,
        'receitas': get_receitas(q, "search"),
        'receitas_aside': get_receitas(source="best_big"),
        "sugestao_receitas": get_receitas(source="suggestions"),
        }
    return render(request, 'listar_pesquisa.html', data)

def cadastrar_usuario(request):
    '''
        Cria um formulário para o cadastro de um novo usuário.
    '''
    data = {
        "sugestao_receitas": get_receitas(source="suggestions"),
    }
    return render(request, 'cadastro_usuario.html', data)

@login_required
def cadastrar_receita(request):
    '''
        Cria um formulário para o cadastro de uma nova receita.
    '''

    try:
        usuario = Usuario.objects.get(dj_usuario=request.user.id)
    except Usuario.DoesNotExist:
        return HttpResponseServerError()

    nova_receita = Receita(autor=usuario)
    nova_receita.save()
    data = {
    "receitas_aside": get_receitas(source="recent"),
    "sugestao_receitas": get_receitas(source="suggestions"),
    "receita_id": nova_receita.id,
    'autor': usuario.nome_completo.title(),
    }
    return render(request, 'cadastro_receita.html', data)

def mapa_site(request):
    '''
        Cria uma página com as URLs mais relevantes da aplicação.
    '''

    data = {
    "sugestao_receitas": get_receitas(source="suggestions"),
    'receitas_aside': get_receitas(source="best_small"),
    }
    return render(request, 'mapa_site.html', data)

def licencas(request):
    '''
        Cria uma página com atribuindo autoria aos componentes de software utilizados.
    '''

    data = {
    "sugestao_receitas": get_receitas(source="suggestions"),
    }
    return render(request, 'licencas.html')

def teste(request):
    '''
        Um easter egg.
    '''
    return render(request, 'teste.html')
