from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
import os
import uuid

# Create your models here.

class Usuario(models.Model):
    dj_usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    nome_completo = models.CharField(max_length=50)
    data_nascimento = models.DateField()
    cidade = models.CharField(max_length=50)
    estado = models.CharField(max_length=2)
    telefone = models.CharField(max_length=15)

    def __str__(self):
        return self.nome_completo + ' (' + self.dj_usuario.username + ')'

class Categoria(models.Model):
    nome_categoria = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.nome_categoria

class ReceitaCategoria(models.Model):
     receita = models.ForeignKey('Receita', on_delete=models.CASCADE)
     categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE)

     def __str__(self):
         return self.receita.nome_receita + ' (' + self.receita.autor.dj_usuario.username + ') faz parte da categoria de ' + self.categoria.nome_categoria

class MetodoCozimento(models.Model):
    nome_metodo = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.nome_metodo

class Unidade(models.Model):
    nome_unidade = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.nome_unidade

class Ingrediente(models.Model):
    nome_ingrediente = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.nome_ingrediente

class ReceitaIngrediente(models.Model):
     receita = models.ForeignKey('Receita', on_delete=models.CASCADE)
     ingrediente = models.ForeignKey('Ingrediente', on_delete=models.CASCADE)
     quantidade = models.DecimalField(max_digits=10, decimal_places=1, default=0)
     unidade = models.ForeignKey('Unidade', on_delete=models.CASCADE)

     def __str__(self):
         return self.receita.nome_receita + ' (' + self.receita.autor.dj_usuario.username + ') tem ' + self.ingrediente.nome_ingrediente

class Receita(models.Model):
    autor = models.ForeignKey('Usuario', on_delete=models.CASCADE)
    nome_receita = models.CharField(max_length=50, null=True)
    descricao = models.TextField(max_length=300, null=True)
    tempo_preparo = models.IntegerField(null=True)
    instrucoes_preparo = models.TextField(max_length=800, null=True)
    porcoes = models.IntegerField(null=True)
    valor_nutricional = models.IntegerField(null=True)
    metodo_cozimento = models.ForeignKey('MetodoCozimento', on_delete=models.CASCADE, null=True)
    categorias = models.ManyToManyField('Categoria', through='ReceitaCategoria')
    ingredientes = models.ManyToManyField('Ingrediente', through='ReceitaIngrediente')
    data_publicacao = models.DateTimeField(auto_now_add=True)
    imagem_padrao = models.ForeignKey('Imagem', related_name='+', on_delete=models.CASCADE, blank=True, null=True)
    media_notas = models.DecimalField(max_digits=2, decimal_places=1, default=0)
    qtd_votos = models.IntegerField(default=0)

    def __str__(self):
        return str(self.nome_receita) + ' (' + str(self.autor.dj_usuario.username) + ')'

def update_filename(instance, filename):
    '''
        Atualiza o nome do arquivo enviado com um nome único.
    '''
    path = "uploads/img/"
    name, extension = os.path.splitext(filename)
    format = str(instance.receita.id) + '-' + str(uuid.uuid4()) + extension
    return os.path.join(path, format)

class Imagem(models.Model):
    file = models.ImageField(upload_to=update_filename)
    imagem_alt = models.CharField(max_length=256)
    receita = models.ForeignKey('Receita', on_delete=models.CASCADE)

    def __str__(self):
        return '"' + str(self.file) + '" de ' + self.receita.nome_receita + ' (' + self.receita.autor.dj_usuario.username + ')'

class Comentario(models.Model):
    autor = models.ForeignKey(User, on_delete=models.CASCADE)
    nota = models.IntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(0)]
    )
    receita = models.ForeignKey('Receita', on_delete=models.CASCADE)
    data_publicacao = models.DateTimeField(auto_now_add=True)
    comentario = models.TextField(max_length=300)

    def __str__(self):
        return 'Comentário de ' + self.autor.username + ' para a receita ' + self.receita.nome_receita + ' (' + self.receita.autor.dj_usuario.username + ')'
