from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Categoria)
admin.site.register(ReceitaCategoria)
admin.site.register(MetodoCozimento)
admin.site.register(Unidade)
admin.site.register(Ingrediente)
admin.site.register(ReceitaIngrediente)
admin.site.register(Imagem)
admin.site.register(Receita)
admin.site.register(Comentario)
