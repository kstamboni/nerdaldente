from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^cadastrar-usuario/$', views.cadastrar_usuario, name='cadastrar_usuario'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^receita/(?P<pk>\d+)$', views.detalhes_receita, name='detalhes_receita'),
    url(r'^imprimir/(?P<pk>\d+)$', views.imprimir_receita, name='views.imprimir_receita'),
    url(r'^cadastrar-receita/$', views.cadastrar_receita, name='cadastrar_receita'),
    url(r'^categoria/(?P<string>[\w\- ]+)/$', views.categoria_receita, name='categoria_receita'),
    url(r'^pesquisa/$', views.pesquisar, name='pesquisar'),
    url(r'^mapa/$', views.mapa_site, name='mapa_site'),
    url(r'^licencas/$', views.licencas, name='licencas'),
    url(r'^teste/$', views.teste, name='teste'),
    url(r'^$', views.index, name='index'),
]
