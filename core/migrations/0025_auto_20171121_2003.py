# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-21 20:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_auto_20171121_1945'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receitaingrediente',
            name='quantidade',
            field=models.DecimalField(decimal_places=1, default=0, max_digits=10),
        ),
    ]
