# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-30 18:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20171027_1735'),
    ]

    operations = [
        migrations.AddField(
            model_name='categoria',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ingrediente',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='metodocozimento',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='unidade',
            name='id',
            field=models.AutoField(auto_created=True, default=1, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='categoria',
            name='nome_categoria',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name='ingrediente',
            name='nome_ingrediente',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name='metodocozimento',
            name='nome_metodo',
            field=models.CharField(max_length=50, unique=True),
        ),
        migrations.AlterField(
            model_name='unidade',
            name='nome_unidade',
            field=models.CharField(max_length=50, unique=True),
        ),
    ]
