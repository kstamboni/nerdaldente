# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-26 19:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('nome_categoria', models.CharField(max_length=50, primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Ingrediente',
            fields=[
                ('nome_ingrediente', models.CharField(max_length=50, primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='MetodoCozimento',
            fields=[
                ('nome_metodo', models.CharField(max_length=50, primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Receita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome_receita', models.CharField(max_length=50)),
                ('descricao', models.TextField(max_length=300)),
                ('tempo_preparo', models.IntegerField()),
                ('instrucoes_preparo', models.TextField(max_length=800)),
                ('porcoes', models.IntegerField()),
                ('valor_nutricional', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ReceitaIngrediente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantidade', models.IntegerField()),
                ('ingrediente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Ingrediente')),
                ('receita', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Receita')),
            ],
        ),
        migrations.CreateModel(
            name='Unidade',
            fields=[
                ('nome_unidade', models.CharField(max_length=50, primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('login', models.CharField(max_length=256)),
                ('nome_completo', models.CharField(max_length=50)),
                ('data_nascimento', models.DateField()),
                ('cidade', models.CharField(max_length=50)),
                ('estado', models.CharField(max_length=2)),
                ('telefone', models.CharField(max_length=15)),
                ('senha', models.CharField(max_length=64)),
            ],
        ),
        migrations.AddField(
            model_name='receitaingrediente',
            name='unidade',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Unidade'),
        ),
        migrations.AddField(
            model_name='receita',
            name='autor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Usuario'),
        ),
        migrations.AddField(
            model_name='receita',
            name='categoria',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Categoria'),
        ),
        migrations.AddField(
            model_name='receita',
            name='ingredientes',
            field=models.ManyToManyField(through='core.ReceitaIngrediente', to='core.Ingrediente'),
        ),
        migrations.AddField(
            model_name='receita',
            name='metodo_cozimento',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.MetodoCozimento'),
        ),
    ]
