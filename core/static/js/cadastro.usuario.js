function verificar_nome()
{
  var nome = $("#inputNome").val();

  if(!nome)
  {
    $("#inputNomeGroup").addClass("has-error");
    $("#inputNomeError").removeClass("hidden");
    $("#inputNomeError").text("Por favor, digite seu nome completo sem abreviações.");
    return false;
  }
  else
  {
    $("#inputNomeGroup").removeClass("has-error");
    $("#inputNomeError").addClass("hidden");
    $("#inputNomeError").text("");
  }

  return true;
}

function verificar_nascimento()
{
  var data_nascimento = $("#inputNascimento").val();

  if(!data_nascimento)
  {
    $("#inputNascimentoGroup").addClass("has-error");
    $("#inputNascimentoError").removeClass("hidden");
    $("#inputNascimentoError").text("Por favor, informe sua data de nascimento.");
    return false;
  }
  else
  {
    $("#inputNascimentoGroup").removeClass("has-error");
    $("#inputNascimentoError").addClass("hidden");
    $("#inputNascimentoError").text("");
  }

  var arr = data_nascimento.split("-");
  var hoje = new Date();
  var idade;

  nsc_ano = parseInt(arr[0]);
  nsc_mes = parseInt(arr[1]) - 1;
  nsc_dia = parseInt(arr[2]);

  hj_dia = hoje.getDate();
  hj_mes = hoje.getMonth();
  hj_ano= hoje.getFullYear();

  if ((hj_mes > nsc_mes) || ( hj_mes === nsc_mes & hj_dia >= nsc_dia))
    idade = hj_ano - nsc_ano;
  else
    idade = hj_ano - nsc_ano - 1;

  if (idade < 13)
  {
    $("#inputNascimentoGroup").addClass("has-error");
    $("#inputNascimentoError").removeClass("hidden");
    $("#inputNascimentoError").text("É necessário ter pelo menos 13 anos para utilizar o Nerd al Dente.");
    return false;
  }
  else
  {
    $("#inputNascimentoGroup").removeClass("has-error");
    $("#inputNascimentoError").addClass("hidden");
    $("#inputNascimentoError").text("");
  }

  return true;
}

function verificar_cidade()
{
  var cidade = $("#inputCidade").val();
  if(!cidade)
  {
    $("#inputCidadeGroup").addClass("has-error");
    $("#inputCidadeError").removeClass("hidden");
    $("#inputCidadeError").text("Por favor, digite a cidade em que você reside atualmente.");
    return false;
  }
  else
  {
    $("#inputCidadeGroup").removeClass("has-error");
    $("#inputCidadeError").addClass("hidden");
    $("#inputCidadeError").text("");
  }

  return true;
}

function verificar_estado()
{
  var estado = $("#inputEstado").val();

  if(estado === "VD")
  {
    $("#inputEstadoGroup").addClass("has-error");
    $("#inputEstadoError").removeClass("hidden");
    $("#inputEstadoError").text("Por favor, informe o estado em que você reside atualmente.");
    return false;
  }
  else
  {
    $("#inputEstadoGroup").removeClass("has-error");
    $("#inputEstadoError").addClass("hidden");
    $("#inputEstadoeError").text("");
  }

  return true;
}

function verificar_telefone()
{
  var cidade = $('#inputTelefone').val();
  if(!cidade)
  {
    $("#inputTelefoneGroup").addClass("has-error");
    $("#inputTelefoneError").removeClass("hidden");
    $("#inputTelefoneError").text("Por favor, digite um telefone para contato.");
    return false;
  }
  else
  {
    $("#inputTelefoneGroup").removeClass("has-error");
    $("#inputTelefoneError").addClass("hidden");
    $("#inputTelefoneError").text("");
  }

  return true;
}

function validar_email($email)
{
  var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return regex.test($email);
}

function autenticar_email($email)
{

  var token = $("input[name=csrfmiddlewaretoken]").val();

  return $.ajax(
   {
     type: 'POST',
     url: '/ajax/validar-usuario/',
     data:
     {
       'csrfmiddlewaretoken': token,
       'email': $email
     },
     dataType: 'json',
     async: false
   });
}

function verificar_email()
{
  var email = $("#inputEmail").val();

  if(!email)
  {
    $("#inputEmailGroup").addClass("has-error");
    $("#inputEmailError").removeClass("hidden");
    $("#inputEmailError").text("Por favor, digite o e-mail que será usado para fazer login no Nerd al Dente.");
    return false;
  }
  else
  {
    $("#inputEmailGroup").removeClass("has-error");
    $("#inputEmailError").addClass("hidden");
    $("#inputEmailError").text("");
  }

  if (!(validar_email(email)))
  {
    $("#inputEmailGroup").addClass("has-error");
    $("#inputEmailError").removeClass("hidden");
    $("#inputEmailError").text("Esse e-mail não é válido. Tentar novamente?");
    return false;
  }
  else
  {
    $("#inputEmailGroup").removeClass("has-error");
    $("#inputEmailOKError").addClass("hidden");
    $("#inputEmailError").text("");
  }

  var result = autenticar_email(email);

  console.log(result.responseJSON);

  if (result.responseJSON.data.existe)
  {
    $("#inputEmailGroup").addClass("has-error");
    $("#inputEmailError").removeClass("hidden");
    $("#inputEmailError").text("Esse e-mail já foi registrado por outra pessoa. Tentar novamente?");
    return false;
  }
  else
  {
    $("#inputEmailGroup").removeClass("has-error");
    $("#inputEmailError").addClass("hidden");
    $("#inputEmailError").text("");
  }

  return true;
}

function verificar_senha()
{
  var senha = $("#inputSenha").val();
  var erros = 0;

  if(!senha)
  {
    $("#inputSenhaGroup").addClass("has-error");
    $("#inputSenhaError").removeClass("hidden");
    $("#inputSenhaError").text("Por favor, digite sua senha para fazer login no Nerd al Dente.");
    return false;
  }
  else
  {
    $("#inputSenhaGroup").removeClass("has-error");
    $("#inputSenhaError").addClass("hidden");
    $("#inputSenhaError").text("");
  }

  $("#inputSenhaErrors").empty();

  if (!(senha.length >= 8))
  {
    $("#inputSenhaErrors").append("<li>Pelo menos 8 caracteres</li>");
    erros++;
  }

  if (!(senha.match(/[a-z]/)))
  {
    $("#inputSenhaErrors").append("<li>Pelo menos uma letra minúscula</li>");
    erros++;
  }

  if (!(senha.match(/[A-Z]/)))
  {
    $("#inputSenhaErrors").append("<li>Pelo menos uma letra maiúscula</li>");
    erros++;
  }

  if (!(senha.match(/\d/)))
  {
    $("#inputSenhaErrors").append("<li>Pelo menos um número</li>");
    erros++;
  }

  if (!(senha.match(/[:;,.!?@#$%\^&*(){}[\]<>/|\-=+_]/)))
  {
    $("#inputSenhaErrors").append("<li>Pelo menos um caractere especial (como !, ?, @, etc.)</li>");
    erros++;
  }

  if (erros > 0)
  {
    $("#inputSenhaGroup").addClass("has-error");
    $("#inputSenhaAlert").removeClass("hidden");
    return false;
  }
  else
  {
    $("#inputSenhaGroup").removeClass("has-error");
    $("#inputSenhaAlert").addClass("hidden");
  }

  return true;
}

function verificar_confirma_senha()
{
  var senha1 = $("#inputSenha").val();
  var senha2 = $("#inputConfirmaSenha").val();

  if(!senha2)
  {
    $("#inputConfirmaSenhaGroup").addClass("has-error");
    $("#inputConfirmaSenhaError").removeClass("hidden");
    $("#inputConfirmaSenhaError").text("Por favor, digite sua senha novamente para ser validada.");
    return false;
  }
  else
  {
    $("#inputConfirmaSenhaGroup").removeClass("has-error");
    $("#inputConfirmaSenhaError").addClass("hidden");
    $("#inputConfirmaSenhaError").text("");
  }

  if (senha1 != senha2)
  {
    $("#inputConfirmaSenhaGroup").addClass("has-error");
    $("#inputConfirmaSenhaError").removeClass("hidden");
    $("#inputConfirmaSenhaError").text("As senhas não coincidem. Tentar novamente?");
    return false;
  }
  else
  {
    $("#inputConfirmaSenhaGroup").removeClass("has-error");
    $("#inputConfirmaSenhaError").addClass("hidden");
    $("#inputConfirmaSenhaError").text("");
  }

  return true;
}

function cadastrar()
{
  var nome_ok = verificar_nome();
  var dt_ok = verificar_nascimento();
  var cidade_ok = verificar_cidade();
  var estado_ok = verificar_estado();
  var telefone_ok = verificar_telefone();
  var email_ok = verificar_email();
  var senha_ok = verificar_senha();
  var c_senha_ok = verificar_confirma_senha();

  var nome = $("#inputNome").val();
  var dt_nascimento = $("#inputNascimento").val();
  var cidade = $("#inputCidade").val();
  var telefone = $("#inputTelefone").val();
  var email = $("#inputEmail").val();
  var senha = $("#inputSenha").val();
  var token = $("input[name=csrfmiddlewaretoken]").val();
  var estado = $('#inputEstado :selected').val();

  if(nome_ok && dt_ok && cidade_ok && estado_ok && telefone_ok && email_ok && senha_ok && c_senha_ok)
  {
    $.ajax(
      {
        type: 'POST',
        url: '/ajax/cadastrar-usuario/',
        data:
        {
          'csrfmiddlewaretoken': token,
          'nome': nome,
          'dt_nascimento': dt_nascimento,
          'cidade': cidade,
          'estado': estado,
          'telefone': telefone,
          'email': email,
          'senha': senha
        },
        dataType: 'json',
        success: function (response)
        {
          console.log(response);

          if (response.data.ok)
            window.location.replace("/?nu=ok");
        }
      });
  }
}

$(document).ready(function()
{
  if ($('#aviso').children().length > 0)
    $("#aviso").removeClass("hidden");

  $("#inputTelefone").mask('(00) 0000-0000');
});

$('#inputNome').keyup(function()
{
  this.value = this.value.toUpperCase();
});

$('#inputNome').focusout(function()
{
  verificar_nome();
});

$('#inputNascimento').focusout(function()
{
  verificar_nascimento();
});

$("#inputNascimento").change(function ()
{
  verificar_nascimento();
});

$('#inputCidade').keyup(function()
{
  this.value = this.value.toUpperCase();
});

$('#inputCidade').focusout(function()
{
  verificar_cidade();
});

$('#inputEstado').focusout(function()
{
  verificar_estado();
});

$('#inputTelefone').focusout(function()
{
  verificar_telefone();
});

$('#inputEmail').focusout(function()
{
  verificar_email();
});

$("#inputEmail").change(function ()
{
  verificar_email();
});

$('#inputSenha').focusout(function()
{
  verificar_senha();
});

$("#inputSenha").change(function ()
{
  verificar_senha();
});

$('#inputConfirmaSenha').focusout(function()
{
  verificar_confirma_senha();
});

$('#inputConfirmaSenha').change(function()
{
  verificar_confirma_senha();
});

$(":button[type='button']").click(function ()
{
  cadastrar();
});

$("#cadastro-form").children().keypress(function(e) {
  if(e.which === 13)
    cadastrar();
});
