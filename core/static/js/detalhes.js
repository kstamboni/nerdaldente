$(document).ready(function()
{
  param = get_url_param("from");

  if (param)
  {
    $('#aviso').append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $('#aviso').append('<strong>Legal!</strong> Agora você fez o login e pode fazer comentários sobre essa receita.');
    $('#aviso').removeClass('hidden');
  }
});

$(document).on("input", "#id_comentario", function () {
  $("#inputComentarioRestantes").text(300 - $(this).val().length);
});

$('#exportar').click(function()
{

  var comentario;
  var content;
  var qtd;
  var i;
  var id;

  id = $('#id_receita').text();

  content = '<?xml version="1.0" encoding="utf-8"?><cookbook version="46"><recipe>';
  content += '<title>' + $('#nome_receita').text() + '</title>';
  content += '<quantity>' + $('#porcoes').text() + '</quantity>';
  content += '<preptime>' + $('#tempo_preparo').text() + ' min</preptime>';
  content += '<nutrition><li>' + $('#calorias').text() + ' kcal</li></nutrition>';

  qtd = $("#categorias").children().length - 1;
  for (i = 1; i <= qtd; i++)
    content += '<category>' + $('#categoria' + i).text() + '</category>';

  qtd = $("#ingredientes").children().length;
  content += '<ingredient>'
  for (i = 1; i <= qtd; i++)
    content += '<li>' + $('#ingrediente' + i).text() + '</li>';
  content += '</ingredient>';

  content += '<description><li>' + $('#descricao').text() + '</li></description>';
  content += '<recipetext>' + $('#instrucoes').text() + '</recipetext>';

  i = 1;
  while(true)
  {
    comentario = $('#comentario' + i).text();
    i++;

    if(comentario === "")
      break;

    content += '<comments>' + comentario + '</comments>';
  }

  content += '<rating>' + $('#nota').text() + '</rating>';
  content += '<imageurl>' + $('#url_img').text() + '</imageurl>';
  content += '<url>' + $('#url_receita').text() + '</url>';
  content += '</recipe></cookbook>';
  console.log(content);

  download("receita_" + id + "_nerdaldente.xml", content);
});

function download(filename, text)
{
  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/xml;charset=utf-8,' + encodeURIComponent(text));
  pom.setAttribute('download', filename);

  if (document.createEvent)
  {
    var event = document.createEvent('MouseEvents');
    event.initEvent('click', true, true);
    pom.dispatchEvent(event);
  }
  else
  {
    pom.click();
  }
}
