var n_ing = 1;
var n_img = 0;

$(document).ready(function()
{
  param = get_url_param("from");

  if (param)
  {
    $('#aviso').append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $('#aviso').append('<strong>Legal!</strong> Agora você fez o login e pode compartilhar mais uma receita no Nerd al Dente.');
    $('#aviso').removeClass('hidden');
  }
});

$(document).on("input", "#inputDescricao", function () {
  $("#inputDescricaoRestantes").text(300 - $(this).val().length);
});

$(document).on("input", "#inputInstrucoes", function () {
  $("#inputInstrucoesRestantes").text(800 - $(this).val().length);
});

function verificar_nome_receita()
{
  var nome_receita = $("#inputReceita").val();

  if(!nome_receita)
  {
    $("#inputReceitaGroup").addClass("has-error");
    $("#inputReceitaError").removeClass("hidden");
    $("#inputReceitaError").text("Por favor, dê um nome à sua receita.");
    return false;
  }
  else
  {
    $("#inputReceitaGroup").removeClass("has-error");
    $("#inputReceitaError").addClass("hidden");
    $("#inputReceitaError").text("");
  }

  return true;
}

$('#inputReceita').focusout(function()
{
  verificar_nome_receita();
});

$('#inputReceita').change(function()
{
  verificar_nome_receita();
});

function verificar_categoria()
{
  if ($("#inputCategoriasGroup input:checkbox:checked").length > 0)
  {

    $("#inputCategoriasPanel").removeClass("panel-danger");
    $("#inputCategoriasPanel").addClass("panel-info");
    $("#inputCategoriasError").addClass("hidden");
    $("#inputCategoriasError").text("");
  }
  else
  {
    $("#inputCategoriasPanel").removeClass("panel-info");
    $("#inputCategoriasPanel").addClass("panel-danger");
    $("#inputCategoriasError").removeClass("hidden");
    $("#inputCategoriasError").html('<br>' + "Escolha pelo menos uma categoria.");
    return false;
  }

  return true;
}

$(':checkbox').change(function()
{
  verificar_categoria();
});

function novo_ingrediente()
{
  n_ing++;
  var novo_ing = '\
  <div class="col-md-5">\
    <label class="sr-only" for="inputIngrediente' + n_ing + '">Nome do ingrediente</label>\
    <input type="text" id="inputIngrediente' + n_ing + '" class="form-control" placeholder="Ex: Ovo" required>\
  </div>\
  <div class="col-md-3">\
    <label class="sr-only" for="inputQuantidade' + n_ing + '">Quantidade</label>\
    <input type="number" step="0.1" id="inputQuantidade' + n_ing + '" class="form-control" placeholder="Ex: 2" required>\
  </div>\
  <div class="col-md-4">\
    <label class="sr-only" for="inputUnidade' + n_ing + '">Unidade</label>\
    <select class="form-control" id="inputUnidade' + n_ing + '">\
      <option value="VD"></option>\
      <option value="VD"></option>\
      <option value="cálice(s)">cálice(s)</option>\
      <option value="colher(es) de café">colher(es) de café</option>\
      <option value="colher(es) de chá">colher(es) de chá</option>\
      <option value="colher(es) de sobremesa">colher(es) de sobremesa</option>\
      <option value="colher(es) de sopa">colher(es) de sopa</option>\
      <option value="copo(s)">copo(s)</option>\
      <option value="copo(s) americano(s)">copo(s) americano(s)</option>\
      <option value="copo(s) de requeijão">copo(s) de requeijão</option>\
      <option value="dúzia(s)">dúzia(s)</option>\
      <option value="g">g</option>\
      <option value="kg">kg</option>\
      <option value="l">l</option>\
      <option value="ml">ml</option>\
      <option value="pitada">pitada</option>\
      <option value="pote(s) grande(s)">pote(s) grande(s)</option>\
      <option value="pote(s) pequeno(s)">pote(s) pequeno(s)</option>\
      <option value="pouco">pouco</option>\
      <option value="unidade(s)">unidade(s)</option>\
      <option value="xícara(s)">xícara(s)</option>\
    </select>\
  </div>\
  <div class="col-md-12"><br></div>'


  $("#ingrediente" + n_ing).append(novo_ing);
  $("#ingrediente" + n_ing).after('<div id="ingrediente' + (n_ing + 1) + '"></div>');
}

function verificar_ingredientes()
{
  var i;
  var ingrediente;
  var quantidade;
  var unidade;
  var ok = true;

  for (i = 1; i <= n_ing; i++)
  {
    ingrediente = $("#inputIngrediente" + i).val();
    quantidade = $("#inputQuantidade" + i).val();
    unidade = $("#inputUnidade" + i).val();

    if(!ingrediente || !quantidade || quantidade <= 0 || unidade === "VD")
    {
      ok = false;
      break;
    }
  }

  if(ok)
  {
    $("#inputIngredientesPanel").removeClass("panel-danger");
    $("#inputIngredientesPanel").addClass("panel-info");
    $("#inputIngredientesError").addClass("hidden");
    $("#inputIngredientesError").text("");
  }
  else
  {
    $("#inputIngredientesPanel").removeClass("panel-info");
    $("#inputIngredientesPanel").addClass("panel-danger");
    $("#inputIngredientesError").removeClass("hidden");
    $("#inputIngredientesError").text("Todos os campos devem ser preenchidos corretamente.");
    return false;
  }

  return true;

}

$("#inputMais").click(function ()
{
  verificar_ingredientes();
  novo_ingrediente();
});


$("#inputIngredientesGroup").focusout(function ()
{
  verificar_ingredientes();
});

function verificar_descricao()
{
  var descricao = $("#inputDescricao").val();

  if(!descricao)
  {
    $("#inputDescricaoGroup").addClass("has-error");
    $("#inputDescricaoError").removeClass("hidden");
    $("#inputDescricaoError").text("Diga às outras pessoas o que torna essa receita especial e por que elas deveriam fazê-la.");
    return false;
  }
  else
  {
    $("#inputDescricaoGroup").removeClass("has-error");
    $("#inputDescricaoError").addClass("hidden");
    $("#inputDescricaoError").text("");
  }

  return true;
}

$("#inputDescricao").focusout(function ()
{
  verificar_descricao();
});

$("#inputDescricao").change(function ()
{
  verificar_descricao();
});

function verificar_instrucoes()
{
  var instrucoes = $("#inputInstrucoes").val();

  if(!instrucoes)
  {
    $("#inputInstrucoesGroup").addClass("has-error");
    $("#inputInstrucoesError").removeClass("hidden");
    $("#inputInstrucoesError").text("Descreva claramente todos os passos de devem ser realizados.");
    return false;
  }
  else
  {
    $("#inputInstrucoesGroup").removeClass("has-error");
    $("#inputInstrucoesError").addClass("hidden");
    $("#inputInstrucoesError").text("");
  }

  return true;
}

$("#inputInstrucoes").focusout(function ()
{
  verificar_instrucoes();
});

$("#inputInstrucoes").change(function ()
{
  verificar_instrucoes();
});

function verificar_tempo_preparo()
{
  var tempo_preparo = $("#inputTempo").val();

  if(!tempo_preparo)
  {
    $("#inputTempoGroup").addClass("has-error");
    $("#inputTempoError").removeClass("hidden");
    $("#inputTempoError").text("Por favor, informe o tempo de preparo estimado da receita.");
    return false;
  }
  else if(tempo_preparo == 0)
  {
    $("#inputTempoGroup").addClass("has-error");
    $("#inputTempoError").removeClass("hidden");
    $("#inputTempoError").html("Isso que é fast food! Tem certeza?");
    return false;
  }
  else if(tempo_preparo < 0)
  {
    $("#inputTempoGroup").addClass("has-error");
    $("#inputTempoError").removeClass("hidden");
    $("#inputTempoError").html("Parece que sua receita pertence ao mundo invertido. Tem certeza?");
    return false;
  }
  else
  {
    $("#inputTempoGroup").removeClass("has-error");
    $("#inputTempoError").addClass("hidden");
    $("#inputTempoError").text("");
  }

  return true;
}

$("#inputTempo").focusout(function ()
{
  verificar_tempo_preparo();
});

$("#inputTempo").change(function ()
{
  verificar_tempo_preparo();
});

function verificar_porcoes()
{
  var porcoes = $("#inputPorcoes").val();

  if(!porcoes)
  {
    $("#inputPorcoesGroup").addClass("has-error");
    $("#inputPorcoesError").removeClass("hidden");
    $("#inputPorcoesError").text("Por favor, informe a quantidade de porções estimada da receita.");
    return false;
  }
  else if(porcoes == 0)
  {
    $("#inputPorcoesGroup").addClass("has-error");
    $("#inputPorcoesError").removeClass("hidden");
    $("#inputPorcoesError").html("Ué... ninguém? Tem certeza?");
    return false;
  }
  else if(porcoes < 0)
  {
    $("#inputPorcoesGroup").addClass("has-error");
    $("#inputPorcoesError").removeClass("hidden");
    $("#inputPorcoesError").html("Parece que sua receita pertence ao mundo invertido. Tem certeza?");
    return false;
  }
  else
  {
    $("#inputPorcoesGroup").removeClass("has-error");
    $("#inputPorcoesError").addClass("hidden");
    $("#inputPorcoesError").text("");
  }

  return true;
}

$("#inputPorcoes").focusout(function ()
{
  verificar_porcoes();
});

$("#inputPorcoes").change(function ()
{
  verificar_porcoes();
});

function verificar_valor_nutricional()
{
  var valor_nutricional = $("#inputNutricional").val();

  if(!valor_nutricional)
  {
    $("#inputNutricionalGroup").addClass("has-error");
    $("#inputNutricionalError").removeClass("hidden");
    $("#inputNutricionalError").text("Por favor, informe a quantidade de calorias estimada da receita.");
    return false;
  }
  else if(valor_nutricional == 0)
  {
    $("#inputNutricionalGroup").addClass("has-error");
    $("#inputNutricionalError").removeClass("hidden");
    $("#inputNutricionalError").html("Bem light mesmo. Tem certeza?");
    return false;
  }
  else if(valor_nutricional < 0)
  {
    $("#inputNutricionalGroup").addClass("has-error");
    $("#inputNutricionalError").removeClass("hidden");
    $("#inputNutricionalError").html("Parece que sua receita pertence ao mundo invertido. Tem certeza?");
    return false;
  }
  else
  {
    $("#inputNutricionalGroup").removeClass("has-error");
    $("#inputNutricionalError").addClass("hidden");
    $("#inputNutricionalError").text("");
  }

  return true;
}

$("#inputNutricional").focusout(function ()
{
  verificar_valor_nutricional();
});

$("#inputNutricional").change(function ()
{
  verificar_valor_nutricional();
});

function verificar_metodo()
{
  var metodo = $("#inputMetodo").val();

  if(metodo === "VD")
  {
    $("#inputMetodoGroup").addClass("has-error");
    $("#inputMetodoError").removeClass("hidden");
    $("#inputMetodoError").text("Por favor, informe o método principal de cozimento da sua receita.");
    return false;
  }
  else
  {
    $("#inputMetodoGroup").removeClass("has-error");
    $("#inputMetodoError").addClass("hidden");
    $("#inputMetodoError").text("");
  }

  return true;
}

$('#inputMetodo').focusout(function()
{
  verificar_metodo();
});

$('#inputMetodo').change(function()
{
  verificar_metodo();
});

$(":button[type='button']").click(function ()
{
  cadastrar();
});

function verificar_imagens()
{
  if(n_img > 0)
  {
    $("#inputImagensPanel").removeClass("panel-danger");
    $("#inputImagensPanel").addClass("panel-info");
    $("#inputImagensError").empty();
    $("#inputImagensError").html('<br>' + '<span>' + n_img + ' imagem(ns) enviada(s).</span>');
  }
  else
  {
    $("#inputImagensPanel").removeClass("panel-info");
    $("#inputImagensPanel").addClass("panel-danger");
    $("#inputImagensError").empty();
    $("#inputImagensError").html('<br>' + '<span style="color: #a94442;">Envie ao menos uma imagem.</span>');
    return false;
  }

  return true;
}

$("#cadastro-form").children().keypress(function(e) {
  if(e.which === 13)
    cadastrar();
});

$("#inputImagens").click(function ()
{
  $("#fileupload").click();
  $("#inputImagensError").empty();
});

$("#fileupload").fileupload(
{
  dataType: 'json',
  done: function (e, response)
  {
    if (response.result.data.ok)
    {
      $("#inputImagensError").append('<span class="label label-success">' + response.result.data.mensagem + '</span><br>');
      n_img++;
    }
    else
      $("#inputImagensError").append('<span class="label label-danger">' + response.result.data.mensagem + '</span><br>');
  }
});

function get_ingredientes()
{
  var ingredientes = [];
  var i;
  var nome_ingrediente;
  var quantidade;
  var unidade;

  for(i = 1; i <= n_ing; i++)
  {
    nome_ingrediente = $("#inputIngrediente" + i).val();
    quantidade = $("#inputQuantidade" + i).val();
    unidade = $('#inputUnidade' + i + ' :selected').val();

    ingredientes.push({
      "nome_ingrediente" : nome_ingrediente,
      "quantidade" : quantidade,
      "unidade" : unidade
    });
  }
  return ingredientes;
  }

function get_categorias()
{
  var input = $("#inputCategoriasGroup input:checkbox:checked");
  var i;
  var categorias = [];

  for (i = 0; i < input.length; i++)
    categorias.push(input[i].value);

  return categorias;
}

function cadastrar()
{
  var ok_nome = verificar_nome_receita();
  var ok_categoria = verificar_categoria();
  var ok_ingredientes = verificar_ingredientes();
  var ok_descricao = verificar_descricao();
  var ok_instrucoes = verificar_instrucoes();
  var ok_tempo = verificar_tempo_preparo();
  var ok_porcoes = verificar_porcoes();
  var ok_nutricional = verificar_valor_nutricional();
  var ok_metodo = verificar_metodo();
  var ok_imagens = verificar_imagens();

  var nome_receita = $("#inputReceita").val();
  var descricao = $("#inputDescricao").val();
  var instrucoes = $("#inputInstrucoes").val();
  var tempo = $("#inputTempo").val();
  var porcoes = $("#inputPorcoes").val();
  var nutricional = $("#inputNutricional").val();
  var metodo = $('#inputMetodo :selected').val();
  var token = $("#csrfmiddlewaretoken").text();
  var receita_id = $("#receita_id").text();

  var ingredientes = get_ingredientes();
  var categorias = get_categorias();

  if (ok_nome && ok_categoria && ok_ingredientes && ok_instrucoes && ok_tempo && ok_porcoes && ok_nutricional && ok_metodo && ok_imagens)
  {
      $.ajax(
        {
          type: 'POST',
          url: '/ajax/cadastrar-multiplos/',
          data:
          {
            'csrfmiddlewaretoken': token,
            'id': receita_id,
            'ingredientes': JSON.stringify(ingredientes),
            'categorias': categorias,
          },
          dataType: 'json',
          success: function (response)
          {
            console.log(response);
          }
        });

        $.ajax(
        {
          type: 'POST',
          url: '/ajax/cadastrar-receita/',
          data:
          {
            'csrfmiddlewaretoken': token,
            'id': receita_id,
            'nome_receita': nome_receita,
            'descricao': descricao,
            'instrucoes': instrucoes,
            'tempo': tempo,
            'porcoes': porcoes,
            'nutricional': nutricional,
            'metodo': metodo
          },
          dataType: 'json',
          success: function (response)
          {
            console.log(response);
            
            if (response.data.ok)
              window.location.replace("/?nr=ok");
          }
        });
  }
}
