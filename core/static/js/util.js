function get_url_param($param)
{
  var url = window.location.search.substring(1);
  var url_variaveis = url.split('&');

  for (var i = 0; i < url_variaveis.length; i++)
  {
    var nome_parametro = url_variaveis[i].split('=');

    if (nome_parametro[0] === $param)
    {
      return nome_parametro[1];
    }

  }
}
