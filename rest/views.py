from django.shortcuts import render, redirect
from django.http import Http404, HttpResponseServerError
from core.models import *
import random
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.db.models import Count
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from django.db.models import Count, Avg
from django.http import JsonResponse
from django.core import serializers
from .util.jsend import *
from json import *
import json
from django.core.serializers.json import DjangoJSONEncoder

# Create your views here.

def get_receitas(query="", source="index"):
    '''
        Método auxiliar para obtenção de uma lista de receitas em vários contextos da aplicação.
    '''
    if source == "index":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).values('nome_receita',
                                                                              'autor__dj_usuario__first_name',
                                                                              'autor__dj_usuario__last_name',
                                                                              'imagem_padrao__file',
                                                                              'imagem_padrao__imagem_alt',
                                                                              'qtd_votos',
                                                                              'media_notas').filter(categorias=query).order_by('-data_publicacao')[:3]
        if len(receitas) < 3:
            return None
        return receitas
    elif source == "cat":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).values('nome_receita',
                                                                              'autor__dj_usuario__first_name',
                                                                              'autor__dj_usuario__last_name',
                                                                              'imagem_padrao__file',
                                                                              'imagem_padrao__imagem_alt',
                                                                              'qtd_votos',
                                                                              'media_notas').filter(categorias=query).order_by('-data_publicacao')[:10]
        return receitas
    elif source == "search":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).filter(Q(autor__nome_completo__icontains=query) | Q(nome_receita__icontains=query) |
                                          Q(descricao__icontains=query) | Q(instrucoes_preparo__icontains=query) |
                                          Q(metodo_cozimento__nome_metodo__icontains=query) | Q(categorias__nome_categoria__icontains=query) |
                                          Q(ingredientes__nome_ingrediente__icontains=query)).distinct().values('nome_receita',
                                                                                                                'autor__dj_usuario__first_name',
                                                                                                                'autor__dj_usuario__last_name',
                                                                                                                'imagem_padrao__file',
                                                                                                                'imagem_padrao__imagem_alt',
                                                                                                                'qtd_votos',
                                                                                                                'media_notas').order_by('-data_publicacao')[:10]
        return receitas
    elif source == "best_small":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).values('nome_receita',
                                                                              'media_notas').order_by('-media_notas')[:10]
        return receitas
    elif source == "best_big":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).values('nome_receita',
                                                                              'descricao',
                                                                              'qtd_votos',
                                                                              'media_notas').order_by('-media_notas')[:5]
        return receitas
    elif source == "recent":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).values('nome_receita',
                                                                               'descricao').order_by('-data_publicacao')[:5]
        return receitas
    elif source == "suggestions":
        receitas = Receita.objects.exclude(imagem_padrao__isnull=True).values('nome_receita').distinct().order_by('-media_notas', '-data_publicacao')[:30]
        return receitas
    else:
        return None

def get_usuarios(type):
    '''
        Obter os usuários mais ativos no site para as listas na página inicial.
    '''
    if type == "degustadores":
        usuarios = Comentario.objects.exclude(receita__imagem_padrao__isnull=True).values('autor__first_name', 'autor__last_name').annotate(total=Count('autor')).order_by('-total')[:5]
        return usuarios
    elif type == "chefs":
        usuarios = Receita.objects.exclude(imagem_padrao__isnull=True).values('autor__dj_usuario__first_name', 'autor__dj_usuario__last_name').annotate(total=Count('autor')).order_by('-total')[:5]
        return usuarios
    return None

# JSON
def index(request):
    '''
        Retorna um JSON da página inicial com todos os dados necessários.
    '''
    categorias = Categoria.objects.all()
    vet = random.sample(range(0, Categoria.objects.count() - 1), 4)

    if get_receitas(categorias[vet[0]]) is not None:
        q = json.dumps(list(get_receitas(categorias[vet[0]])), cls=DjangoJSONEncoder)
        r1 = json.loads(q)
    else:
        r1 = None

    if get_receitas(categorias[vet[1]]) is not None:
        q = json.dumps(list(get_receitas(categorias[vet[1]])), cls=DjangoJSONEncoder)
        r2 = json.loads(q)
    else:
        r2 = None

    if get_receitas(categorias[vet[2]]) is not None:
        q = json.dumps(list(get_receitas(categorias[vet[2]])), cls=DjangoJSONEncoder)
        r3 = json.loads(q)
    else:
        r3 = None

    if get_receitas(categorias[vet[3]]) is not None:
        q = json.dumps(list(get_receitas(categorias[vet[3]])), cls=DjangoJSONEncoder)
        r4 = json.loads(q)
    else:
        r4 = None

    q = json.dumps(list(get_receitas(source="suggestions")), cls=DjangoJSONEncoder)
    sug = json.loads(q)

    q = json.dumps(list(get_usuarios("chefs")), cls=DjangoJSONEncoder)
    chef = json.loads(q)

    q = json.dumps(list(get_usuarios("degustadores")), cls=DjangoJSONEncoder)
    deg = json.loads(q)

    if request.user.is_authenticated():
        user = request.user.first_name
    else:
        user = None

    data = {
        "categoria1": categorias[vet[0]].nome_categoria,
        "receitas1": r1,
        "categoria2": categorias[vet[1]].nome_categoria,
        "receitas2": r2,
        "categoria3": categorias[vet[2]].nome_categoria,
        "receitas3": r3,
        "categoria4": categorias[vet[3]].nome_categoria,
        "receitas4": r4,
        "login_usuario": user,
        "sugestao_receitas": sug,
        "chefs": chef,
        "degustadores": deg,
        }

    return JsonResponse(JSend.success(data))

def detalhes_receita(request, pk):
    '''
        Retorna um JSON com todos os detalhes de uma receita, incluindo as URLs de imagens e comentários recebidos.
    '''
    try:
        receita = Receita.objects.exclude(imagem_padrao__isnull=True).get(id=pk)
    except Receita.DoesNotExist:
        return JsonResponse(JSend.error("Não conseguimos encontrar essa receita.", 404))

    q = json.dumps(list(Receita.objects.filter(id=pk).values('nome_receita',
                                                             'descricao',
                                                             'instrucoes_preparo',
                                                             'tempo_preparo',
                                                             'porcoes',
                                                             'valor_nutricional',
                                                             'metodo_cozimento__nome_metodo',
                                                             'data_publicacao',
                                                             'media_notas',
                                                             'qtd_votos')), cls=DjangoJSONEncoder)
    rec = json.loads(q)

    q = json.dumps(list(Imagem.objects.filter(receita=receita).values('file','imagem_alt')), cls=DjangoJSONEncoder)
    imagens = json.loads(q)

    q = json.dumps(list(Comentario.objects.filter(receita=receita).values('autor__first_name',
                                                                          'autor__last_name',
                                                                          'comentario',
                                                                          'data_publicacao')), cls=DjangoJSONEncoder)
    comentarios = json.loads(q)

    q = json.dumps(list(ReceitaCategoria.objects.filter(receita=receita).values('categoria__nome_categoria')), cls=DjangoJSONEncoder)
    categorias = json.loads(q)

    q = json.dumps(list(ReceitaIngrediente.objects.filter(receita=receita).values('ingrediente__nome_ingrediente',
                                                                                  'quantidade',
                                                                                  'unidade__nome_unidade')), cls=DjangoJSONEncoder)
    ingredientes = json.loads(q)

    q = json.dumps(list(get_receitas(source="suggestions")), cls=DjangoJSONEncoder)
    sug = json.loads(q)

    if request.user.is_authenticated():
        user = request.user.first_name
    else:
        user = None

    data = {
        'id_receita': pk,
        'url': "https://nerdaldente.com.br/receita/" + pk,
        'receita': rec,
        'autor': receita.autor.nome_completo.title(),
        'imagens': imagens,
        'comentarios': comentarios,
        'categorias': categorias,
        'ingredientes': ingredientes,
        'login_usuario': user,
        'sugestao_receitas': sug,
        }

    return JsonResponse(JSend.success(data))

def imprimir_receita(request, pk):
    '''
        Retorna um JSON da página para ser impressa com os detalhes mais relevantes de uma receita.
    '''

    try:
        receita = Receita.objects.exclude(imagem_padrao__isnull=True).get(id=pk)
    except Receita.DoesNotExist:
        return JsonResponse(JSend.error("Não conseguimos encontrar essa receita.", 404))

    q = json.dumps(list(Receita.objects.filter(id=pk).values('nome_receita',
                                                             'descricao',
                                                             'instrucoes_preparo',
                                                             'tempo_preparo',
                                                             'porcoes',
                                                             'valor_nutricional',
                                                             'metodo_cozimento__nome_metodo',
                                                             'data_publicacao',
                                                             'media_notas',
                                                             'qtd_votos',
                                                             'imagem_padrao__file',
                                                             'imagem_padrao__imagem_alt')), cls=DjangoJSONEncoder)
    rec = json.loads(q)

    q = json.dumps(list(ReceitaCategoria.objects.filter(receita=receita).values('categoria__nome_categoria')), cls=DjangoJSONEncoder)
    categorias = json.loads(q)

    q = json.dumps(list(ReceitaIngrediente.objects.filter(receita=receita).values('ingrediente__nome_ingrediente',
                                                                                  'quantidade',
                                                                                  'unidade__nome_unidade')), cls=DjangoJSONEncoder)
    ingredientes = json.loads(q)

    data = {
        'id_receita': pk,
        'url': "https://nerdaldente.com.br/imprimir/" + pk,
        'receita': rec,
        'autor': receita.autor.nome_completo.title(),
        'categorias': categorias,
        'ingredientes': ingredientes,
        }

    return JsonResponse(JSend.success(data))

def categoria_receita(request, string):
    '''
        Retorna um JSON com a lista das receitas mais recentes de uma categoria.
    '''
    try:
        categoria = Categoria.objects.get(nome_categoria=string.title())
    except Categoria.DoesNotExist:
        return JsonResponse(JSend.error("Não conseguimos encontrar essa categoria.", 404))

    q = json.dumps(list(get_receitas(categoria, "cat")), cls=DjangoJSONEncoder)
    receitas = json.loads(q)

    q = json.dumps(list(get_receitas(source="best_big")), cls=DjangoJSONEncoder)
    aside = json.loads(q)

    q = json.dumps(list(get_receitas(source="suggestions")), cls=DjangoJSONEncoder)
    sug = json.loads(q)

    if request.user.is_authenticated():
        user = request.user.first_name
    else:
        user = None

    data = {
        'categoria': string.title(),
        'url': "https://nerdaldente.com.br/categoria/" + string,
        'receitas': receitas,
        'receitas_aside': aside,
        'login_usuario': user,
        'sugestao_receitas': sug,
        }

    return JsonResponse(JSend.success(data))

def pesquisar(request):
    '''
        Retorna um JSON com a lista das receitas que se adequam à string de pesquisa do usuário.
    '''

    if request.GET.get('q'):
        query = str(request.GET.get('q')).lower()
    else:
        return JsonResponse(JSend.fail("Requisição com problemas. Verifique se o parâmetro 'q' é válido."))

    q = json.dumps(list(get_receitas(query, "search")), cls=DjangoJSONEncoder)
    receitas = json.loads(q)

    q = json.dumps(list(get_receitas(source="best_big")), cls=DjangoJSONEncoder)
    aside = json.loads(q)

    q = json.dumps(list(get_receitas(source="suggestions")), cls=DjangoJSONEncoder)
    sug = json.loads(q)

    if request.user.is_authenticated():
        user = request.user.first_name
    else:
        user = None

    data = {
        'q': query,
        'url': "https://nerdaldente.com.br/pesquisar/" + query,
        'receitas': receitas,
        'receitas_aside': aside,
        'login_usuario': user,
        'sugestao_receitas': sug,
    }

    return JsonResponse(JSend.success(data))

def sugestoes(request):
    '''
        Retorna um JSON com a lista de sugestões para o campo de pesquisa da aplicação.
    '''
    q = json.dumps(list(get_receitas(source="suggestions")), cls=DjangoJSONEncoder)
    sug = json.loads(q)

    if request.user.is_authenticated():
        user = request.user.first_name
    else:
        user = None

    data = {
        'login_usuario': user,
        'sugestao_receitas': sug,
        }

    return JsonResponse(JSend.success(data))

def cadastrar_receita(request):
    '''
        Retorna um JSON com todos os dados necessários para a criação do formulário de cadastro de uma nova receita.
    '''

    try:
        usuario = Usuario.objects.get(dj_usuario=request.user.id)
    except Usuario.DoesNotExist:
        return JsonResponse(JSend.error("Falha em identificar o usuário da requisição.", 500))

    nova_receita = Receita(autor=usuario)
    nova_receita.save()

    q = json.dumps(list(get_receitas(source="recent")), cls=DjangoJSONEncoder)
    aside = json.loads(q)

    q = json.dumps(list(get_receitas(source="suggestions")), cls=DjangoJSONEncoder)
    sug = json.loads(q)

    if request.user.is_authenticated():
        user = request.user.first_name
    else:
        user = None

    data = {
        'receitas_aside': aside,
        'receita_id': nova_receita.id,
        'autor': usuario.nome_completo.title(),
        'login_usuario': user,
        'sugestao_receitas': sug,
        }

    return JsonResponse(JSend.success(data))

def mapa_site(request):
    '''
        Retorna um JSON com as URLs mais relevantes da aplicação.
    '''

    q = json.dumps(list(get_receitas(source="best_small")), cls=DjangoJSONEncoder)
    aside = json.loads(q)

    q = json.dumps(list(get_receitas(source="suggestions")), cls=DjangoJSONEncoder)
    sug = json.loads(q)

    categorias = ['acompanhamentos', 'aperitivos', 'entradas', 'lanches', 'molhos', 'pratos principais', 'saladas', 'sobremesas']
    q = json.dumps(categorias)
    cat = json.loads(q)

    if request.user.is_authenticated():
        user = request.user.first_name
    else:
        user = None

    data = {
        'receitas_aside': aside,
        'login_usuario': user,
        'sugestao_receitas': sug,
        'link_cadastrar_receita': '/cadastrar-receita',
        'link_login': '/login',
        'link_cadastrar_usuario': '/cadastrar-usuario',
        'link_categoria': '/categoria/',
        'categorias': cat,
        }

    return JsonResponse(JSend.success(data))
