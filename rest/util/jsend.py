from rest_framework.renderers import JSONRenderer

STATUS_FIELD = "status"
DATA_FIELD = "data"
MESSAGE_FIELD = "message"
CODE_FIELD = "code"

SUCCESS_STATUS = "success"
ERROR_STATUS = "error"
FAIL_STATUS = "fail"

class JSend:
    '''
        Contém métodos que formata os dados em conformidade com https://labs.omniti.com/labs/jsend.
    '''
    
    def success(data):
        return {STATUS_FIELD: SUCCESS_STATUS,
                DATA_FIELD: data}

    def fail(data):
        return {STATUS_FIELD: FAIL_STATUS,
                DATA_FIELD: data}

    def error(message, code=None, data=None):
        if(code != None and data != None):
            return {STATUS_FIELD: ERROR_STATUS,
                    MESSAGE_FIELD: message,
                    CODE_FIELD: code,
                    DATA_FIELD: data}
        elif(code == None and data == None):
                return {STATUS_FIELD: ERROR_STATUS,
                        MESSAGE_FIELD: message}
        elif(code == None):
            return {STATUS_FIELD: ERROR_STATUS,
                    MESSAGE_FIELD: message,
                    DATA_FIELD: data}
        else:
            return {STATUS_FIELD: ERROR_STATUS,
                    MESSAGE_FIELD: message,
                    CODE_FIELD: code}
