# Nerd Al Dente

Um cat�logo web de receitas desenvolvido por alunos do ICMC-USP usando Django.

## Autores
Ana Caroline Spengler, Kleber Roberto Stamboni e Pedro Felipe Coloma de Ara�jo.

## Apresenta��o
Seguimos as especifica��es propostas na implementa��o do site de receitas (arquivo anexo). Como utilizamos a renderiza��o de p�gina do lado do servidor, tamb�m fornecemos uma interface de resposta no formato JSON em conformidade com o padr�o JSend (labs.omniti.com/labs/jsend). O projeto � divido em tr�s partes principais: core (que comp�e os templates, os models e outros arquivos est�ticos), ajax (que recebe os cadastros assincronamente, incluindo os arquivos de imagem) e rest (que converte as estruturas de dados do Python utilizadas nos templates para o formato JSON serializ�vel). Com exce��o dos coment�rios e do formul�rio de login, os cadastros s�o realizados por meio de AJAX. Todos os plugins utilizados s�o descritos na p�gina de licen�as (http://127.0.0.1:8000/licencas/) e outros detalhes sobre eles podem ser vistos no relat�rio anexo. Tamb�m disponibilizamos um tutorial para instala��o logo a seguir (testado e executado no Ubuntu 16.04 LTS).

## Como executar o sistema localmente
### Siga os seguintes passos
Ap�s descompactar o arquivo, crie um ambiente virtual
```sh
python3 -m venv env
```
Ative o ambiente virtual
```sh
source env/bin/activate
```
Acesse a pasta do projeto
```sh
cd nerdaldente
```
Instale os pacotes utilizados no projeto
```sh
pip install -r requirements.txt
```
Execute o servidor local
```sh
python manage.py runserver
```

## Para ser administrador
Crie um superusu�rio
```sh
python manage.py createsuperuser
```
Acesse a p�gina de administrador
```sh
http://127.0.0.1:8000/admin/
```

## Para reiniciar a base de dados
Exclua a base de dados atual
```sh
python manage.py flush
```
Sincronize a base de dados com os modelos definidos
```sh
python manage.py migrate
```
Coloque algumas caracter�sticas das receitas na base de dados
```sh
python manage.py populate_db
```
